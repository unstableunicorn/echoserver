package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
)

var version = "0.0.0"

func main() {
	portPtr := flag.Int64("p", 4206, "set the port number")
	flag.Parse()

	http.HandleFunc("/", handleRoot)
	http.HandleFunc("/echo", handleEcho)

	// safetly exit
	sigchan := make(chan os.Signal, 1)
	signal.Notify(sigchan, os.Interrupt, syscall.SIGTERM)

	log.Printf("echo server version: v%v\n", version)
	log.Printf("serving on localhost:%v\n", *portPtr)
	go func() {
		log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", *portPtr), nil))
	}()
	s := <-sigchan
	log.Printf("finished serving by: %v\n", s)
}

// handleRoot just prints the body to the console
func handleRoot(w http.ResponseWriter, r *http.Request) {
	req, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf("could not parse body: %v", err), http.StatusBadRequest)
		return
	}
	log.Printf("got req: %s", req)
}

// handleEcho return the same body and content type to the sender
func handleEcho(w http.ResponseWriter, r *http.Request) {
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, fmt.Sprintf("could not parse body: %v", err), http.StatusBadRequest)
		return
	}
	contentType := r.Header.Get("content-type")
	if contentType != "" {
		w.Header().Set("content-type", contentType)
	}
	log.Printf("got req: %s", data)

	w.Header().Set("Content-Length", fmt.Sprintf("%v", len(data)))
	_, err = w.Write(data)
	if err != nil {
		http.Error(w, fmt.Sprintf("failed to write reponse: %v", err), http.StatusInternalServerError)
	}
}
